package com.kenfogel.javafxmulticontainerv4_stack.view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.kenfogel.fishfxhtml.view.FishFXHTMLControllerDND;
import com.kenfogel.fishfxtable.view.FishFXTableController;
import com.kenfogel.fishfxtree.view.FishFXTreeControllerDND;
import com.kenfogel.javafxmulticontainerv4_stack.MainAppFX;
import com.kenfogel.javafxmulticontainerv4_stack.persistence.FishDAO;
import com.kenfogel.javafxmulticontainerv4_stack.persistence.FishDAOImpl;
import javafx.application.Platform;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the root layout. All of the other layouts are added in code here.
 * This allows us to use the standalone containers with minimal changes.
 *
 * i18n added
 *
 * Added proper logging
 *
 * @author Ken Fogel
 * @version 1.2
 *
 */
public class RootLayoutStackController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutStackController.class);

    @FXML
    private ResourceBundle resources;

    @FXML // fx:id="rootStackPane"
    private StackPane rootStackPane; // Value injected by FXMLLoader

    @FXML // fx:id="stack1btn"
    private Button stack1btn; // Value injected by FXMLLoader

    @FXML // fx:id="stack2btn"
    private Button stack2btn; // Value injected by FXMLLoader

    @FXML // fx:id="stack3btn"
    private Button stack3btn; // Value injected by FXMLLoader

    @FXML // fx:id="stack4btn"
    private Button stack4btn; // Value injected by FXMLLoader

    private AnchorPane treeView;
    private AnchorPane tableView;
    private AnchorPane webView;
    private AnchorPane htmlView;

    private final FishDAO fishDAO;
    private FishFXTreeControllerDND fishFXTreeController;
    private FishFXTableController fishFXTableController;
    //private FishFXWebViewController fishFXWebViewController;
    private FishFXHTMLControllerDND fishFXHTMLController;

    public RootLayoutStackController() {
        fishDAO = new FishDAOImpl();
    }

    /**
     * Here we call upon the methods that load the other containers and then
     * send the appropriate action command to each container
     */
    @FXML
    private void initialize() {

        initTreeLayout();
        initTableLayout();
        initWebViewLayout();
        initHtmlLayout();

        // Tell the tree about the table
        setTableControllerToTree();

        try {
            fishFXTreeController.displayTree();
            fishFXTableController.displayTheTable();
            //fishFXHTMLController.displayFishAsHTML();
        } catch (SQLException ex) {
            LOG.error("initialize error", ex);
            errorAlert("initialize()");
            Platform.exit();
        }
    }

    /**
     * Send the reference to the FishFXTableController to the
     * FishFXTreeController
     */
    private void setTableControllerToTree() {
        fishFXTreeController.setTableController(fishFXTableController);
    }

    /**
     * The TreeView Layout
     */
    private void initTreeLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutStackController.class
                    .getResource("/fxml/FishFXTreeLayout.fxml"));
            treeView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXTreeController = loader.getController();
            fishFXTreeController.setFishDAO(fishDAO);

            rootStackPane.getChildren().add(treeView);
        } catch (IOException ex) {
            LOG.error("initUpperLeftLayout error", ex);
            errorAlert("initUpperLeftLayout()");
            Platform.exit();
        }
    }

    /**
     * The TableView Layout
     */
    private void initTableLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutStackController.class
                    .getResource("/fxml/FishFXTableLayout.fxml"));
            tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXTableController = loader.getController();
            fishFXTableController.setFishDAO(fishDAO);

            rootStackPane.getChildren().add(tableView);
        } catch (SQLException | IOException ex) {
            LOG.error("initUpperRightLayout error", ex);
            errorAlert("initUpperRightLayout()");
            Platform.exit();
        }
    }

    /**
     * The WebView Layout
     */
    private void initWebViewLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutStackController.class
                    .getResource("/fxml/FishFXWebViewLayout.fxml"));
            webView = (AnchorPane) loader.load();

            // Retrieve the controller if you must send it messages
            //fishFXWebViewController = loader.getController();
            rootStackPane.getChildren().add(webView);
        } catch (IOException ex) {
            LOG.error("initLowerLeftLayout error", ex);
            errorAlert("initLowerLeftLayout()");
            Platform.exit();
        }
    }

    /**
     * The HTMLEditor Layout
     */
    private void initHtmlLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutStackController.class
                    .getResource("/fxml/FishFXHTMLLayout.fxml"));
            htmlView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXHTMLController = loader.getController();
            fishFXHTMLController.setFishDAO(fishDAO);

            rootStackPane.getChildren().add(htmlView);
        } catch (IOException ex) {
            LOG.error("initLowerRightLayout error", ex);
            errorAlert("initLowerRightLayout()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }

    /**
     * Select the treeView
     * @param event 
     */
    @FXML
    void bringStack1Forward(ActionEvent event) {
        treeView.toFront();
        treeView.setVisible(true);
        closeOtherPanes(treeView);

    }

    /**
     * Select the tableView
     * @param event 
     */
    @FXML
    void bringStack21Forward(ActionEvent event) {
        tableView.toFront();
        tableView.setVisible(true);
        closeOtherPanes(tableView);

    }

    /**
     * Select the webView
     * @param event 
     */
    @FXML
    void bringStack3Forward(ActionEvent event) {
        webView.toFront();
        webView.setVisible(true);
        closeOtherPanes(webView);

    }

    /**
     * Select the htmlView
     * @param event 
     */
    @FXML
    void bringStack4Forward(ActionEvent event) {
        htmlView.toFront();
        htmlView.setVisible(true);
        closeOtherPanes(htmlView);

    }

    /**
     * setVisible to false for all but the pane parameter
     *
     * @param paneTokeep The pane to preserve
     */
    private void closeOtherPanes(Node paneTokeep) {
        rootStackPane.getChildren().stream().filter((pane) -> (pane != paneTokeep)).forEachOrdered((pane) -> {
            pane.setVisible(false);
        });
    }

}
